<%-- 
    Document   : cadProduto
    Created on : 15/11/2014, 16:30:17
    Author     : Gianfranco
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Cadastro de Produto</title>
    </head>    
    <body>
        <form action="/PetShop/SvProduto" method="POST">            
            NOME:<input type="text" name="nome" value="" /><br>
            MARCA:<input type="text" name="marca" value="" /><br>
            QUANTIDADE:<input type="text" name="quantidade" value="" /><br>
            QUANTIDADE MINIMA EM ESTOQUE:<input type="text" name="quantidade_minima" value="" /><br>
            PREÇO DE COMPRA:<input type="text" name="preco_compra" value="" /><br>
            PREÇO DE VENDA:<input type="text" name="preco_venda" value="" /><br>           
            <input type="hidden" name="ACAO" value="INSERIR" />
            <input type="submit" value="Cadastrar" />         
        </form>        
         <%
            String id = request.getParameter("ins");
            
            if (id != null)
                out.println("USER INSERIDO COM SUCESSO. ID: "+id);
            
            %>
    </body>
</html>
