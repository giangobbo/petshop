<%-- 
    Document   : alterarProduto
    Created on : 17/11/2014, 14:10:36
    Author     : Gianfranco
--%>

<%@page import="model.Produto"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean class="model.DAOProduto" id="daop" scope="page"></jsp:useBean>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Alteracao de Produtos</title>
    </head>
    <body>
        
        <%  String codigo=" ",cod=" ",n=" ",m=" ",q=" ",pc=" ",pv=" ",qm=" ";            
            Produto p;
            codigo=request.getParameter("cod_prod");
           if(codigo!=null){
               p=daop.getById(codigo);
               cod=String.valueOf(p.getCod_prod());
               n=p.getNome();
               m=p.getMarca();
               q=String.valueOf(p.getQuantidade());
               pc=String.valueOf(p.getPreco_compra());
               pv=String.valueOf(p.getPreco_venda());   
               qm=String.valueOf(p.getQuantidade_minima());
                                     
           }
        
        %>
            <form action="/PetShop/SvProduto" method="POST">          
                        
            CODIGO DO PRODUTO:<input type="text" name="cod_prod" value="<%=cod%>" /><br><br>            
            PREENCHA APENAS OS CAMPOS QUE DESEJA ALTERAR<br><br>         
            Nome:<input type="text" name="nome" value="<%=n%>" /><br>            
            Marca:<input type="text" name="marca" value="<%=m%>" /><br>
            Quantidade:<input type="text" name="quantidade" value="<%=q%>" /><br>
            Quantidade Minima em Estoque:<input type="text" name="quantidade_minima" value="<%=qm%>" /><br>
            Preco de Compra:<input type="text" name="preco_compra" value="<%=pc%>" /><br>
            Preco de Venda:<input type="text" name="preco_venda" value="<%=pv%>" /><br>   
            
            <input type="hidden" name="ACAO" value="ALTERAR" />
            <input type="submit" value="Alterar" />         
            </form>
    </body>
</html>
