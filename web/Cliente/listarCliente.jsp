<%-- 
    Document   : listarCliente
    Created on : 16/11/2014, 00:14:51
    Author     : Gianfranco
--%>


<%@page import="model.Cliente"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean class="model.DAOCliente" id="du" scope="page"></jsp:useBean>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Lista de Clientes</title>
    </head>
    <body>
        
        <h2>Listagem</h2>
        
        <table border="1">
            <thead>
                <tr>
                    <th></th>
                    <th></th>
                    <th>NOME</th>
                    <th>CPF</th>
                    <th>EMAIL</th>
                    <th>TELEFONE</th>
                    <th>RUA</th>
                    <th>NUMERO</th>
                    <th>CIDADE</th>
                    <th>ESTADO</th>
                </tr>
            </thead>
            <tbody>
                

        <%
            ArrayList<Cliente> au = du.pesquisar("SELECT * FROM cliente");
            
            
            for (Cliente u : au)
            {
            
            String endereco=u.getEndereco();
            
            String enderecovetor[];
            enderecovetor=endereco.split(";");
            
            
        %>
                <tr>
                    <td><a href="/PetShop/SvCliente?ACAO=EXCLUIR&cpf_cliente=<%=u.getCpf_cliente()%>">Excluir</a></td>
                    <td><a href="alterarCliente.jsp?cpf_cliente=<%=u.getCpf_cliente() %>">Alterar</a></td>
                    <td><%=u.getNome_cliente() %></td>
                    <td><%=u.getCpf_cliente() %></td>
                    <td><%=u.getEmail() %></td>
                    <td><%=u.getTelefone() %></td>
                    
                    <td><%=enderecovetor[0]%></td>
                    <td><%=enderecovetor[1]%></td>
                    <td><%=enderecovetor[2]%></td>
                    <td><%=enderecovetor[3]%></td>
  
                </tr>
        <%}%>
            </tbody>
        </table>
        
    </body>
</html>
