<%-- 
    Document   : alterarCliente
    Created on : 15/11/2014, 23:24:54
    Author     : Gianfranco
--%>

<%@page import="model.Cliente"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean class="model.DAOCliente" id="daoc" scope="page"></jsp:useBean>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Alterar Cliente</title>
    </head>
    <body><%
        String cpf_cliente=request.getParameter("cpf_cliente"),enderecovetor[]=null,nome,email,telefone,rua,numero,cidade,uf;
        Cliente c=null;
        c=daoc.getByCpf(cpf_cliente);        
        if (c==null) {
            cpf_cliente="";
            nome="";
            email="";
            telefone="";
            rua="";
            numero="";
            cidade="";
            uf="";
        }
        else {
            
            enderecovetor=c.getEndereco().split(";");
            nome=c.getNome_cliente();
            email=c.getEmail();
            telefone=c.getTelefone();
            rua=enderecovetor[0];
            numero=enderecovetor[1];
            cidade=enderecovetor[2];
            uf=enderecovetor[3];
                }
            
            
            
        
    %>
        <form action="/PetShop/SvCliente" method="POST">     
            
            CPF DO CLIENTE:<input type="text" name="cpf_cliente" value="<%=cpf_cliente%>" /><br><br>            
            PREENCHA APENAS OS CAMPOS QUE DESEJA ALTERAR<br><br>         
            Nome:<input type="text" name="nome" value="<%=nome%>"> <br>            
            Email:<input type="text" name="email" value="<%=email%>" /><br>
            Telefone:<input type="text" name="telefone" value="<%=telefone%>" /><br>
            Rua:<input type="text" name="logradouro" value="<%=rua%>" /><br>
            Numero:<input type="text" name="numero" value="<%=numero%>" /><br>    
            Cidade:<input type="text" name="cidade" value="<%=cidade%>" /><br>
            Estado:<input type="text" name="uf" value="<%=uf%>" /><br>
            <input type="hidden" name="ACAO" value="ALTERAR" />
            <input type="submit" value="Alterar" />         
        </form>
    </body>
</html>
