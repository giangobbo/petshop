/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Cliente;
import model.DAOCliente;
import util.Banco;


/**
 *
 * @author Gianfranco
 */
@WebServlet(name = "SvCliente_1", urlPatterns = {"/SvCliente_1"})
public class SvCliente extends HttpServlet {
    
    private DAOCliente dc;
    
    
    @Override
    public void init()
    {
        dc = new DAOCliente();
        
    }
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        
        String ACAO,scpf_cliente,snome_cliente,semail,stelefone;
        int scod_end;
        
        Banco banco = new Banco();
        
        String suf,snumero,scidade,slogradouro;

        
        ACAO = request.getParameter("ACAO");
        
        if (ACAO.equalsIgnoreCase("INSERIR"))        {
            
            
            suf = request.getParameter("uf");
            snumero = request.getParameter("numero");            
            scidade = request.getParameter("cidade");
            slogradouro = request.getParameter("logradouro");
            String sendereco=slogradouro+";"+snumero+";"+scidade+";"+suf;
            //scpf = request.getParameter("cpf");


            
            
            Cliente c = new Cliente();
            
            scpf_cliente = request.getParameter("cpf_cliente");
            snome_cliente = request.getParameter("nome");            
            semail = request.getParameter("email");
            stelefone = request.getParameter("telefone");
            
            

            c.setCpf_cliente(scpf_cliente);
            c.setEmail(semail);
            c.setNome_cliente(snome_cliente);
            c.setTelefone(stelefone);
            c.setEndereco(sendereco);
            
            
            int id2 = dc.inserir(c);
            
            System.out.println("PK de cliente eh:"+banco.recuperaPk("SELECT * from cliente WHERE cpf_cliente="+c.getCpf_cliente(), "cpf_cliente"));
            
            
            
            response.sendRedirect("Cliente/cadCliente.jsp?ins="+id2);
            
            
            
            //out.print("INSERIDO ID: "+id);
        } else
        if (ACAO.equalsIgnoreCase("EXCLUIR"))
        {
            String cpf = request.getParameter("cpf_cliente");
            
            boolean r = dc.excluir(cpf);
            
            if (r)
              response.sendRedirect("Cliente/listarCliente.jsp");
            
            
        }
        else
        if (ACAO.equalsIgnoreCase("ALTERAR"))
        {
                       
            
            String end = request.getParameter("logradouro")+";"+request.getParameter("numero")+";"+request.getParameter("cidade")+";"+request.getParameter("uf");                 
            String snome = request.getParameter("nome");            
            semail=request.getParameter("email");            
            stelefone = request.getParameter("telefone");
            String scpf = request.getParameter("cpf_cliente");            
            Cliente c = dc.getByCpf(scpf); 
            c.setNome_cliente(snome);
            c.setEmail(semail);
            c.setTelefone(stelefone);
            c.setEndereco(end);
                     
            dc.alterar(c);
            
            response.sendRedirect("Cliente/listarCliente.jsp");
            
        }
        }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
    
