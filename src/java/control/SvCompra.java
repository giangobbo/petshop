/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Compra;
import model.DAOCompra;
import model.DAOProduto;
import model.Produto;

/**
 *
 * @author Gianfranco
 */
@WebServlet(name = "SvCompra", urlPatterns = {"/SvCompra"})
public class SvCompra extends HttpServlet {
    
    private DAOCompra dc;
    private DAOProduto dp;
    
    public void init(){
        dc = new DAOCompra();
        dp = new DAOProduto();

    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            
        String ACAO = request.getParameter("ACAO");
        String smatricula,scnpj,sdata,scod_prod,squantidade;
        
        
        if (ACAO.equalsIgnoreCase("INSERIR"))        {
            
            smatricula=request.getParameter("matricula");
            scnpj=request.getParameter("cnpj");            
            scod_prod=request.getParameter("cod_prod");            
            squantidade=request.getParameter("quantidade");            
            
            
            Compra c = new Compra();

            c.setCnpj(scnpj);            
            c.setData_compra("18/11/2014");
            
            Produto p=dp.getById(scod_prod);
            p.setQuantidade(p.getQuantidade()+Integer.parseInt(squantidade));
            dp.alterar(p);          
                             
            int id = dc.inserir(c);                        
            response.sendRedirect("Compra/cadCompra.jsp?ins="+id);                       
            
            //out.print("INSERIDO ID: "+id);
        } else
        if (ACAO.equalsIgnoreCase("EXCLUIR"))
        {
            String cod = request.getParameter("matricula");
            
            boolean r = dc.excluir(Integer.parseInt(cod));
            
            if (r)
              response.sendRedirect("listarCompra.jsp");
            
            
        }
        else
        if (ACAO.equalsIgnoreCase("ALTERAR"))
        {
            Produto p = new Produto();

            
            smatricula=request.getParameter("matricula");
            scnpj=request.getParameter("cnpj");            
            
            
            Compra c = new Compra();

            c.setCnpj(scnpj);
            c.setMatricula(Integer.parseInt(smatricula));

  
            c.setData_compra("18/11/2014");
            boolean i=dc.alterar(c);  
            
            response.sendRedirect("Produto/listarCompra.jsp");
            
            
        }
            
            
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
