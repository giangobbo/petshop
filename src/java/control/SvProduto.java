/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.DAOProduto;

import model.Produto;

/**
 *
 * @author Gianfranco
 */
public class SvProduto extends HttpServlet {
    
    private DAOProduto dp;
    
    public void init(){
        dp = new DAOProduto();
    }    
    

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            
        String ACAO,snome,smarca,squantidade,sqtdm,scod_prod,spreco_compra,spreco_venda,id1 = null;        
        


        
        ACAO = request.getParameter("ACAO");
        
        if (ACAO.equalsIgnoreCase("INSERIR"))        {
            
            
            snome = request.getParameter("nome");
            smarca = request.getParameter("marca");
            squantidade = request.getParameter("quantidade");
            sqtdm = request.getParameter("quantidade_minima");
            spreco_compra = request.getParameter("preco_compra");
            spreco_venda = request.getParameter("preco_venda");

            Produto p = new Produto();
            p.setMarca(smarca);
            p.setNome(snome);
            p.setPreco_compra(Double.parseDouble(spreco_compra));
            p.setPreco_venda(Double.parseDouble(spreco_venda));
            p.setQuantidade(Integer.parseInt(squantidade));
            p.setQuantidade_minima(Integer.parseInt(sqtdm));
            
            int id = dp.inserir(p);            
            
            response.sendRedirect("Produto/cadProduto.jsp?ins="+id);            
            
            
            //out.print("INSERIDO ID: "+id);
        } else
        if (ACAO.equalsIgnoreCase("EXCLUIR"))
        {
            String cod = request.getParameter("cod_prod");
            
            boolean r = dp.excluir(Integer.parseInt(cod));
            
            if (r)
              response.sendRedirect("Produto/listarProduto.jsp");
            
            
        }
        else
        if (ACAO.equalsIgnoreCase("ALTERAR"))
        {
            Produto p = new Produto();

            
            snome = request.getParameter("nome");
            smarca = request.getParameter("marca");
            squantidade = request.getParameter("quantidade");
            sqtdm = request.getParameter("quantidade_minima");
            scod_prod = request.getParameter("cod_prod");
            spreco_compra = request.getParameter("preco_compra");
            spreco_venda = request.getParameter("preco_venda");


            p.setNome(snome);
            p.setMarca(smarca);
            p.setQuantidade(Integer.parseInt(squantidade));
            p.setQuantidade_minima(Integer.parseInt(sqtdm));
            p.setPreco_compra(Double.parseDouble(spreco_compra));
            p.setPreco_venda(Double.parseDouble(spreco_venda));
            p.setCod_prod(Integer.parseInt(scod_prod));  
            boolean i=dp.alterar(p);            
            response.sendRedirect("Produto/listarProduto.jsp");
            
            
        }
            
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the codp.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
