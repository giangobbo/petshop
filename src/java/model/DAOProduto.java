/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Felipe
 */
import util.*;
import java.util.*;
import java.sql.*;

public class DAOProduto {

    private Banco b;

    public DAOProduto() {
        b = new Banco();
    }

    public int inserir(Produto u) {
        int r = 0;
        
        r = b.inserir("INSERT INTO produto(cod_prod, nome, marca, quantidade, preco_venda, preco_compra,quantidade_minima) VALUES (DEFAULT, '"+u.getNome()+"', '"+u.getMarca()+"',"+u.getQuantidade()+","+u.getPreco_venda()+","+u.getPreco_compra()+","+u.getQuantidade_minima()+")");

        return r;
    }


    public boolean excluir(int id) {
        int r = 0;
        r = b.excluir("DELETE FROM Produto WHERE cod_prod="+ id);

        if (r <= 0) {
            return false;
        }

        return true;

    }

    public ArrayList<Produto> pesquisar(String sql) {
        ArrayList<Produto> vet = new ArrayList<Produto>();

        ResultSet r = b.pesquisar(sql);
        try {
            while (r.next()) {
                Produto u = new Produto();
                u.setCod_prod(r.getInt("cod_prod"));
                u.setNome(r.getString("nome"));
                u.setMarca(r.getString("marca"));
                u.setQuantidade(r.getInt("quantidade"));
                u.setQuantidade_minima(r.getInt("quantidade_minima"));
                u.setPreco_venda(r.getDouble("preco_venda"));
                u.setPreco_compra(r.getDouble("preco_compra"));
                
                vet.add(u);

            }
        } catch (SQLException ex) {
            System.err.println("Erro pesquisa: " + ex);
        }
        return vet;
    }
    
    public boolean alterar(Produto p)
    {
        int r=0;        
              
                      
        r = b.atualizar("UPDATE Produto SET cod_prod='"+p.getCod_prod()+"', nome='"+p.getNome()+"',quantidade="+p.getQuantidade()+",preco_compra="+p.getPreco_compra()+",preco_venda="+p.getPreco_venda()+",quantidade_minima="+p.getQuantidade_minima()+" WHERE cod_prod="+p.getCod_prod());
        
        
        if (r <= 0)
            return false;
        
        return true;
    }

    public Produto getById(String id) {

        ResultSet r = b.pesquisar("SELECT * from Produto WHERE cod_prod = " + id);
        try {
            if (r.next()) {
                Produto u = new Produto();
                u.setCod_prod(r.getInt("cod_prod"));
                u.setNome(r.getString("nome"));
                u.setMarca(r.getString("marca"));
                u.setQuantidade(r.getInt("quantidade"));
                u.setPreco_venda(r.getDouble("preco_venda"));
                u.setPreco_compra(r.getDouble("preco_compra"));
                
                return u;

            }
        } catch (SQLException ex) {
            System.err.println("Erro pesquisa: " + ex);
        }
        return null;
    }

    public void fechar() {
        b.fechar();
    }
}
