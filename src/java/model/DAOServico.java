/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Felipe
 */
import util.*;
import java.util.*;
import java.sql.*;

public class DAOServico {

    private Banco b;

    public DAOServico() {
        b = new Banco();
    }

    public int inserir(Servico u) {
        int r = 0;
        
        r = b.inserir("INSERT INTO Servico(cod_servico, nome,especie,valor,cod_exec) VALUES ('" + u.getCod_servico()+ "', '" + u.getNome() + "','" + u.getEspecie()+ "','" +u.getValor()+"','"+u.getCod_exec()+"'");

        return r;
    }


    public boolean excluir(int id) {
        int r = 0;
        r = b.excluir("DELETE FROM Servico WHERE cod_servico = " + id);

        if (r <= 0) {
            return false;
        }

        return true;

    }

    public ArrayList<Servico> pesquisar(String sql) {
        ArrayList<Servico> vet = new ArrayList<Servico>();

        ResultSet r = b.pesquisar(sql);
        try {
            while (r.next()) {
                Servico u = new Servico();
                u.setCod_servico(r.getInt("cod_servico"));
                u.setNome(r.getString("nome"));
                u.setEspecie(r.getString("especie"));
                u.setValor(r.getDouble("valor"));
                u.setCod_exec(r.getInt("cod_exec"));
                
                
                vet.add(u);

            }
        } catch (SQLException ex) {
            System.err.println("Erro pesquisa: " + ex);
        }
        return vet;
    }

    public Servico getById(String id) {

        ResultSet r = b.pesquisar("SELECT * from Servico WHERE cod_prod = " + id);
        try {
            if (r.next()) {
                Servico u = new Servico();
                u.setCod_servico(r.getInt("cod_servico"));
                u.setNome(r.getString("nome"));
                u.setEspecie(r.getString("especie"));
                u.setValor(r.getDouble("valor"));
                u.setCod_exec(r.getInt("cod_exec"));
                
                return u;

            }
        } catch (SQLException ex) {
            System.err.println("Erro pesquisa: " + ex);
        }
        return null;
    }

    public void fechar() {
        b.fechar();
    }
}
