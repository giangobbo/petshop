/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Felipe
 */
import util.*;
import java.util.*;
import java.sql.*;


public class DAOAnimal 
{
    private Banco b;
    
    public DAOAnimal()
    {
        b = new Banco();
    }
    
    public int inserir(Animal u)
    {
        int r=0;
        
        
             
        r = b.inserir("INSERT INTO Animal(id, nome_animal,especie,raca,carac) VALUES ('"+u.getId()+"', '"+u.getNome_animal()+"','"+u.getEspecie()+"','"+u.getRaca()+"','"+u.getCarac()+"')");
        
        return r;
    }
    
    public boolean alterar(Animal u)
    {
        int r=0;
        
        
               
        r = b.atualizar("UPDATE Animal SET nome_animal='"+u.getNome_animal()+"',especie='"+u.getEspecie()+"',raca='"+u.getRaca()+"',carac='"+u.getCarac());
        
        
        if (r <= 0)
            return false;
        
        return true;
    }

	public boolean excluir(int id)
	{
		int r=0;
		r = b.excluir("DELETE FROM Animal WHERE id = "+id);
		
		if (r <= 0)
			return false;
		
		return true;
		
	}
	
 
    public ArrayList<Animal> pesquisar(String sql)
	{
		ArrayList<Animal> vet = new ArrayList<Animal>();
		
		ResultSet r = b.pesquisar(sql);
                try 
                {
                    while(r.next())
                    {
                        Animal u = new Animal();
                        u.setId(r.getInt("id"));
                        u.setNome_animal(r.getString("nome_animal"));
                        u.setEspecie(r.getString("especie"));
                        u.setRaca(r.getString("raca"));
                        u.setCarac(r.getString("carac"));
                        vet.add(u);

                    }
                } 
                catch (SQLException ex) {
                    System.err.println("Erro pesquisa: "+ex);
                }
		return vet;
	}
    
        public Animal getById(String id)
	{
		
		ResultSet r = b.pesquisar("SELECT * from Animal WHERE id = "+id);
                try 
                {
                    if (r.next())
                    {
                        Animal u = new Animal();
                       
                        u.setId(r.getInt("id"));
                        u.setNome_animal(r.getString("nome_animal"));
                        u.setEspecie(r.getString("especie"));
                        u.setRaca(r.getString("raca"));
                        u.setCarac(r.getString("carac"));

                        return u;

                    }
                } 
                catch (SQLException ex) {
                    System.err.println("Erro pesquisa: "+ex);
                }
		return null;
	}
	
	public void fechar()
	{	
	b.fechar();
	}
}
