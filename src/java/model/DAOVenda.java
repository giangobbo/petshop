/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Felipe
 */
import util.*;
import java.util.*;
import java.sql.*;

public class DAOVenda {

    private Banco b;

    public DAOVenda() {
        b = new Banco();
    }

    public int inserir(Venda u) {
        int r = 0;
        
        r = b.inserir("INSERT INTO Venda(cod_venda, matricula,cpf_cliente,data_venda) VALUES ('" + u.getCod_venda()+ "', '" + u.getMatricula()+ "','" + u.getCpf_cliente()+ "','" +u.getData_venda()+"'");

        return r;
    }


    public boolean excluir(int id) {
        int r = 0;
        r = b.excluir("DELETE FROM Venda WHERE cod_servico = " + id);

        if (r <= 0) {
            return false;
        }

        return true;

    }

    public ArrayList<Venda> pesquisar(String sql) {
        ArrayList<Venda> vet = new ArrayList<Venda>();

        ResultSet r = b.pesquisar(sql);
        try {
            while (r.next()) {
                Venda u = new Venda();
                u.setCod_venda(r.getInt("cod_venda"));
                u.setMatricula(r.getInt("matricula"));
                u.setCpf_cliente(r.getString("cpf_cliente"));
                u.setData_venda(r.getDate("data_venda"));
                
                
                
                
                vet.add(u);

            }
        } catch (SQLException ex) {
            System.err.println("Erro pesquisa: " + ex);
        }
        return vet;
    }

    public Venda getById(String id) {

        ResultSet r = b.pesquisar("SELECT * from Venda WHERE cod_prod = " + id);
        try {
            if (r.next()) {
                Venda u = new Venda();
                u.setCod_venda(r.getInt("cod_venda"));
                u.setMatricula(r.getInt("matricula"));
                u.setCpf_cliente(r.getString("cpf_cliente"));
                u.setData_venda(r.getDate("data_venda"));
                
                return u;

            }
        } catch (SQLException ex) {
            System.err.println("Erro pesquisa: " + ex);
        }
        return null;
    }

    public void fechar() {
        b.fechar();
    }
}
