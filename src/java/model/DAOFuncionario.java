/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author ferrasa
 */
import util.*;
import java.util.*;
import java.sql.*;


public class DAOFuncionario 
{
    private Banco b;
    
    public DAOFuncionario()
    {
        b = new Banco();
    }
    
    public int inserir(Funcionario u)
    {
        int r=0;
        
        String data = "";
             
        r = b.inserir("INSERT INTO Funcionario(matricula, nome_funcionario,telefone,cpf_funcionario,cod_end) VALUES ('"+u.getMatricula()+"', '"+u.getNome_funcionario()+"','"+u.getTelefone()+"','"+u.getCpf_funcionario()+"','"+u.getCod_end()+"')");
        
        return r;
    }
    
    public boolean alterar(Funcionario u)
    {
        int r=0;
        
        String data = "";
               
        r = b.atualizar("UPDATE Funcionario SET cpf_funcionario='"+u.getCpf_funcionario()+"', nome_funcionario='"+u.getNome_funcionario()+"',matricula='"+u.getMatricula()+"',telefone='"+u.getTelefone()+"',cod_end='"+u.getCod_end());
        
        
        if (r <= 0)
            return false;
        
        return true;
    }

	public boolean excluir(int id)
	{
		int r=0;
		r = b.excluir("DELETE FROM Funcionario WHERE cpf_funcionario = "+id);
		
		if (r <= 0)
			return false;
		
		return true;
		
	}
	
 
    public ArrayList<Funcionario> pesquisar(String sql)
	{
		ArrayList<Funcionario> vet = new ArrayList<Funcionario>();
		
		ResultSet r = b.pesquisar(sql);
                try 
                {
                    while(r.next())
                    {
                        Funcionario u = new Funcionario();
                        u.setMatricula(r.getInt("matricula"));
                        u.setNome_funcionario(r.getString("nome_funcionario"));
                        u.setTelefone(r.getString("telefone"));
                        u.setCpf_funcionario(r.getString("cpf_funcionario"));
                        u.setCod_end(r.getInt("cod_end"));
                        vet.add(u);

                    }
                } 
                catch (SQLException ex) {
                    System.err.println("Erro pesquisa: "+ex);
                }
		return vet;
	}
    
        public Funcionario getByCpf(String id)
	{
		
		ResultSet r = b.pesquisar("SELECT * from Funcionario WHERE cpf_funcionario = "+id);
                try 
                {
                    if (r.next())
                    {
                        Funcionario u = new Funcionario();
                       
                       u.setMatricula(r.getInt("matricula"));
                        u.setNome_funcionario(r.getString("nome_funcionario"));
                        u.setTelefone(r.getString("telefone"));
                        u.setCpf_funcionario(r.getString("cpf_funcionario"));
                        u.setCod_end(r.getInt("cod_end"));

                        return u;

                    }
                } 
                catch (SQLException ex) {
                    System.err.println("Erro pesquisa: "+ex);
                }
		return null;
	}
	
	public void fechar()
	{	
	b.fechar();
	}
}
