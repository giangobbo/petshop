/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author ferrasa
 */
import util.*;
import java.util.*;
import java.sql.*;


public class DAOCliente 
{
    private Banco b;
    
    public DAOCliente()
    {
        b = new Banco();
    }
    
    public int inserir(Cliente u)
    {
        int r=0;
        
        String data = "";
        
             
        r = b.inserir("INSERT INTO Cliente(cpf_cliente, nome_cliente,email,telefone,endereco) VALUES ('"+u.getCpf_cliente()+"', '"+u.getNome_cliente()+"','"+u.getEmail()+"','"+u.getTelefone()+"','"+u.getEndereco()+"')");
        
        return r;
    }
    
    public boolean alterar(Cliente u)
    {
        int r=0;
        
        String data = "";
               
        r = b.atualizar("UPDATE Cliente SET cpf_cliente='"+u.getCpf_cliente()+"', nome_cliente='"+u.getNome_cliente()+"',email='"+u.getEmail()+"',telefone='"+u.getTelefone()+"',endereco='"+u.getEndereco()+"' WHERE cpf_cliente='"+u.getCpf_cliente()+"'");
        
        
        if (r <= 0)
            return false;
        
        return true;
    }

	public boolean excluir(String cpf)
	{
		int r=0;
		r = b.excluir("DELETE FROM Cliente WHERE cpf_cliente='"+cpf+"'");
		
		if (r <= 0)
			return false;
		
		return true;
		
	}
	
 
    public ArrayList<Cliente> pesquisar(String sql)
	{
		ArrayList<Cliente> vet = new ArrayList<Cliente>();
		
		ResultSet r = b.pesquisar(sql);
                try 
                {
                    while(r.next())
                    {
                        Cliente u = new Cliente();
                        u.setCpf_cliente(r.getString("cpf_cliente"));
                        u.setNome_cliente(r.getString("nome_cliente"));
                        u.setEmail(r.getString("email"));
                        u.setTelefone(r.getString("telefone"));
                        u.setEndereco(r.getString("endereco"));
                        vet.add(u);

                    }
                } 
                catch (SQLException ex) {
                    System.err.println("Erro pesquisa: "+ex);
                }
		return vet;
	}
    
        public Cliente getByCpf(String cpf_cliente)
	{
		
		ResultSet r = b.pesquisar("SELECT *FROM cliente WHERE cpf_cliente ='"+cpf_cliente+"'");
                try 
                {
                    if (r.next())
                    {
                        Cliente u = new Cliente();
                       
                        u.setCpf_cliente(r.getString("cpf_cliente"));
                        u.setNome_cliente(r.getString("nome_cliente"));
                        u.setEmail(r.getString("email"));
                        u.setTelefone(r.getString("telefone"));
                        u.setEndereco(r.getString("endereco"));

                        return u;

                    }
                } 
                catch (SQLException ex) {
                    System.err.println("Erro pesquisa: "+ex);
                }
		return null;
	}
	
	public void fechar()
	{	
	b.fechar();
	}
}
