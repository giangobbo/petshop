/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Felipe
 */
import util.*;
import java.util.*;
import java.sql.*;

public class DAOCompra {

    private Banco b;

    public DAOCompra() {
        b = new Banco();
    }

    public int inserir(Compra u) {
        int r = 0;
        String data = "";
        

        r = b.inserir("INSERT INTO Compra(cod_compra, matricula,cnpj,data_compra) VALUES ('" + u.getCod_compra() + "', '" + u.getMatricula() + "','" + u.getCnpj() + "','" + "'2014-11-18'");

        return r;
    }

    public boolean alterar(Compra u) {
        int r = 0;
        String data = "";
        r = b.atualizar("UPDATE Compra SET matricula='" + u.getMatricula() + "',cnpj='" + u.getCnpj());

        if (r <= 0) {
            return false;
        }

        return true;
    }

    public boolean excluir(int id) {
        int r = 0;
        r = b.excluir("DELETE FROM Compra WHERE cod_compra = " + id);

        if (r <= 0) {
            return false;
        }

        return true;

    }

    public ArrayList<Compra> pesquisar(String sql) {
        ArrayList<Compra> vet = new ArrayList<Compra>();

        ResultSet r = b.pesquisar(sql);
        try {
            while (r.next()) {
                Compra u = new Compra();
                u.setCod_compra(r.getInt("cod_compra"));
                u.setMatricula(r.getInt("matricula"));
                u.setCnpj(r.getString("especie"));
                u.setData_compra(r.getString("data_compra"));

                vet.add(u);

            }
        } catch (SQLException ex) {
            System.err.println("Erro pesquisa: " + ex);
        }
        return vet;
    }

    public Compra getById(String id) {

        ResultSet r = b.pesquisar("SELECT * from Compra WHERE cod_compra = " + id);
        try {
            if (r.next()) {
                Compra u = new Compra();

                u.setCod_compra(r.getInt("cod_compra"));
                u.setMatricula(r.getInt("matricula"));
                u.setCnpj(r.getString("especie"));
                u.setData_compra(r.getString("data_compra"));

                return u;

            }
        } catch (SQLException ex) {
            System.err.println("Erro pesquisa: " + ex);
        }
        return null;
    }

    public void fechar() {
        b.fechar();
    }
}
