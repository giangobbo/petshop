
package model;

public class Servico {
    private int cod_servico,cod_exec;
    private String nome, especie;
    private double valor;

    public int getCod_servico() {
        return cod_servico;
    }

    public void setCod_servico(int cod_servico) {
        this.cod_servico = cod_servico;
    }

    public int getCod_exec() {
        return cod_exec;
    }

    public void setCod_exec(int cod_exec) {
        this.cod_exec = cod_exec;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEspecie() {
        return especie;
    }

    public void setEspecie(String especie) {
        this.especie = especie;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }
}
