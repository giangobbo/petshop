/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author ferrasa
 */
import util.*;
import java.util.*;
import java.sql.*;


public class DAOFornecedor 
{
    private Banco b;
    
    public DAOFornecedor()
    {
        b = new Banco();
    }
    
    public int inserir(Fornecedor u)
    {
        int r=0;
        
        String data = "";
             
        r = b.inserir("INSERT INTO Fornecedor(cnpj, email,telefone,nomefantasia,cod_end) VALUES ('"+u.getCnpj()+"', '"+u.getEmail()+"','"+u.getTelefone()+"','"+u.getNomefantasia()+"','"+u.getCod_end()+"')");
        
        return r;
    }
    
    public boolean alterar(Fornecedor u)
    {
        int r=0;
        
        String data = "";
               
        r = b.atualizar("UPDATE Fornecedor SET cnpj='"+u.getCnpj()+"', email='"+u.getEmail()+"',telefone='"+u.getTelefone()+"',nomefantasia='"+u.getNomefantasia()+"',cod_end='"+u.getCod_end());
        
        
        if (r <= 0)
            return false;
        
        return true;
    }

	public boolean excluir(int id)
	{
		int r=0;
		r = b.excluir("DELETE FROM Fornecedor WHERE cnpj = "+id);
		
		if (r <= 0)
			return false;
		
		return true;
		
	}
	
 
    public ArrayList<Fornecedor> pesquisar(String sql)
	{
		ArrayList<Fornecedor> vet = new ArrayList<Fornecedor>();
		
		ResultSet r = b.pesquisar(sql);
                try 
                {
                    while(r.next())
                    {
                        Fornecedor u = new Fornecedor();
                        u.setCnpj(r.getString("cnpj"));
                        u.setEmail(r.getString("email"));
                        u.setTelefone(r.getString("telefone"));
                        u.setNomefantasia(r.getString("fantasia"));
                        u.setCod_end(r.getInt("cod_end"));
                        vet.add(u);

                    }
                } 
                catch (SQLException ex) {
                    System.err.println("Erro pesquisa: "+ex);
                }
		return vet;
	}
    
        public Fornecedor getByCpf(String id)
	{
		
		ResultSet r = b.pesquisar("SELECT * from Fornecedor WHERE cnpj = "+id);
                try 
                {
                    if (r.next())
                    {
                        Fornecedor u = new Fornecedor();
                       
                        u.setCnpj(r.getString("cnpj"));
                        u.setEmail(r.getString("email"));
                        u.setTelefone(r.getString("telefone"));
                        u.setNomefantasia(r.getString("fantasia"));
                        u.setCod_end(r.getInt("cod_end"));

                        return u;

                    }
                } 
                catch (SQLException ex) {
                    System.err.println("Erro pesquisa: "+ex);
                }
		return null;
	}
	
	public void fechar()
	{	
	b.fechar();
	}
}
