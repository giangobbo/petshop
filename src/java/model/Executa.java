//modificacao
package model;

import java.sql.Date;

public class Executa {
    private int cod_exec,matricula,id;
    private Date data_exec;

    public int getCod_exec() {
        return cod_exec;
    }

    public void setCod_exec(int cod_exec) {
        this.cod_exec = cod_exec;
    }

    public int getMatricula() {
        return matricula;
    }

    public void setMatricula(int matricula) {
        this.matricula = matricula;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getData_exec() {
        return data_exec;
    }

    public void setData_exec(Date data_exec) {
        this.data_exec = data_exec;
    }
}
