/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Felipe
 */
import util.*;
import java.util.*;
import java.sql.*;

public class DAOExecuta {

    private Banco b;

    public DAOExecuta() {
        b = new Banco();
    }

    public int inserir(Executa u) {
        int r = 0;
        String data = "";
        if (u.getData_exec() != null) {
            data = Tools.formatInsertDataHora(u.getData_exec());
        }

        r = b.inserir("INSERT INTO Executa(cod_exec, matricula,id,data_exec) VALUES ('" + u.getCod_exec()+ "', '" + u.getMatricula() + "','" + u.getId()+ "','" + data);

        return r;
    }


    public boolean excluir(int id) {
        int r = 0;
        r = b.excluir("DELETE FROM Executa WHERE cod_exec = " + id);

        if (r <= 0) {
            return false;
        }

        return true;

    }

    public ArrayList<Executa> pesquisar(String sql) {
        ArrayList<Executa> vet = new ArrayList<Executa>();

        ResultSet r = b.pesquisar(sql);
        try {
            while (r.next()) {
                Executa u = new Executa();
                u.setCod_exec(r.getInt("cod_exec"));
                u.setMatricula(r.getInt("matricula"));
                u.setId(r.getInt("id"));
                u.setData_exec(r.getDate("data_exec"));

                vet.add(u);

            }
        } catch (SQLException ex) {
            System.err.println("Erro pesquisa: " + ex);
        }
        return vet;
    }

    public Executa getById(String id) {

        ResultSet r = b.pesquisar("SELECT * from Executa WHERE cod_exec = " + id);
        try {
            if (r.next()) {
                Executa u = new Executa();

                u.setCod_exec(r.getInt("cod_exec"));
                u.setMatricula(r.getInt("matricula"));
                u.setId(r.getInt("id"));
                u.setData_exec(r.getDate("data_exec"));

                return u;

            }
        } catch (SQLException ex) {
            System.err.println("Erro pesquisa: " + ex);
        }
        return null;
    }

    public void fechar() {
        b.fechar();
    }
}
