<%-- 
    Document   : listarCompra
    Created on : 16/11/2014, 00:14:51
    Author     : Gianfranco
--%>


<%@page import="model.Compra"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean class="model.DAOCompra" id="du" scope="page"></jsp:useBean>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Lista de Compras</title>
    </head>
    <body>
        
        <h2>Listagem</h2>
        
        <table border="1">
            <thead>
                <tr>
                    <th></th>
                    <th></th>
                    <th>CODIGO DA COMPRA</th>
                    <th>MATRICULA</th>
                    <th>CNPJ</th>

                    
                </tr>
            </thead>
            <tbody>
                

        <%
            ArrayList<Compra> au = du.pesquisar("SELECT * FROM compra");
            
            
            for (Compra u : au)
            {            
            
            
            
        %>
                <tr>
                    <td><a href="/PetShop/SvCompra?ACAO=EXCLUIR&cod_compra=<%=u.getCod_compra()%>">Excluir</a></td>
                    <td><a href="alterarCompra.jsp?cod_compra=<%=u.getCod_compra() %>">Alterar</a></td>
                    <td><%=u.getCod_compra()%></td>
                    <td><%=u.getMatricula()%></td>
                    <td><%=u.getCnpj() %></td>

                    
  
                </tr>
        <%}%>
            </tbody>
        </table>
        
    </body>
</html>
