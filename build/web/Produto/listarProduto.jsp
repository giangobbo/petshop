<%-- 
    Document   : listarProduto
    Created on : 16/11/2014, 00:14:51
    Author     : Gianfranco
--%>


<%@page import="model.Produto"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean class="model.DAOProduto" id="du" scope="page"></jsp:useBean>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Lista de Produtos</title>
    </head>
    <body>
        
        <h2>Listagem</h2>
        
        <table border="1">
            <thead>
                <tr>
                    <th></th>
                    <th></th>
                    <th>NOME</th>
                    <th>MARCA</th>
                    <th>QUANTIDADE</th>
                    <th>QUANTIDADE MIN EM ESTOQUE</th>
                    <th>PRECO DE COMPRA</th>
                    <th>PRECO DE VENDA</th>
                    
                </tr>
            </thead>
            <tbody>
                

        <%
            ArrayList<Produto> au = du.pesquisar("SELECT * FROM produto");
            
            
            for (Produto u : au)
            {            
            
            
            
        %>
                <tr>
                    <td><a href="/PetShop/SvProduto?ACAO=EXCLUIR&cod_prod=<%=u.getCod_prod()%>">Excluir</a></td>
                    <td><a href="alterarProduto.jsp?cod_prod=<%=u.getCod_prod() %>">Alterar</a></td>
                    <td><%=u.getNome()%></td>
                    <td><%=u.getMarca()%></td>
                    <td><%=u.getQuantidade() %></td>
                    <td><%=u.getQuantidade_minima()%></td>
                    <td><%=u.getPreco_compra() %></td>                    
                    <td><%=u.getPreco_venda() %></td>
                    
  
                </tr>
        <%}%>
            </tbody>
        </table>
        
    </body>
</html>
